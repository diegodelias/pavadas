# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'principal.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets, uic
from dialog import Ui_Dialog as Form
import sqlite3

class Ui_MainWindow(object):

    """"
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        uic.loadUi('principalVent.ui', self)
        self.show()
         """

    def cargarInfo(self):
        """Funcion para mostra listado de articulos"""
        conexion = sqlite3.connect("base.sqlite")
        consulta = "SELECT * FROM ARTI"
        resultado = conexion.execute(consulta)
        self.tableWidget.setRowCount(0)

        """primer for para la fila y segundo for para las columnas"""
        for fila_num, fila_datos in enumerate(resultado):
            self.tableWidget.insertRow(fila_num)
            for col_num, datos in enumerate(fila_datos):
                self.tableWidget.setItem(fila_num,col_num, QtWidgets.QTableWidgetItem(str(datos)))

        conexion.close()

    def editar(self):
        dialog = QtWidgets.QDialog()
        dialog.ui = Form()
        dialog.ui.setupUi(dialog)
        dialog.exec_()
        dialog.show()




    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(698, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(0, 70, 721, 181))
        self.tableWidget.setRowCount(5)
        self.tableWidget.setColumnCount(7)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.clicked.connect(self.editar)
        self.ListarArticulos = QtWidgets.QPushButton(self.centralwidget)
        self.ListarArticulos.setGeometry(QtCore.QRect(600, 30, 87, 29))
        self.ListarArticulos.setObjectName("ListarArticulos")

        # al hacer click sobre el boton ejecutar metodo cargarInfo
        self.ListarArticulos.clicked.connect(self.cargarInfo)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 698, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("Articulos", "Articulos"))
        self.ListarArticulos.setText(_translate("MainWindow", "Listar Articulos"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
